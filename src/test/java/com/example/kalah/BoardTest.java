package com.example.kalah;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.kalah.model.Board;

@RunWith(SpringJUnit4ClassRunner.class)
public class BoardTest {

	Board board;
	private static final String PLAYER1 = "PLAYER1";
	private static final String PLAYER2 = "PLAYER2";

	@Before
	public void setUp() {
		board = new Board();
	}
	
	@Test
	public void testNewBoard() throws Exception {
		board.startNewGame();
		assertEquals(4, board.getHoles().get(1).intValue());
		assertEquals(0, board.getHoles().get(6).intValue());
	}
	
	@Test
	public void testBasicSeed() throws Exception {
		board.startNewGame();
		board.setTurn(true); // Forcing PLAYER1
		// First move
		board.play(PLAYER1, 0);
		assertEquals(5, board.getHoles().get(4).intValue());
		assertEquals(4, board.getHoles().get(5).intValue());

		board.play(PLAYER2, 7);
		assertEquals(5, board.getHoles().get(11).intValue());
		assertEquals(4, board.getHoles().get(12).intValue());
	}
	
	@Test
	public void testChangeTurn() throws Exception {
		board.startNewGame();
		board.setTurn(true); // Forcing PLAYER1
		board.play(PLAYER1, 0);
		assertEquals(PLAYER2, board.getTurn());
		board.play(PLAYER2, 7);
		assertEquals(PLAYER1, board.getTurn());
	}
	
	@Test
	public void testBasicSeedDoubleTurn() throws Exception {
		board.startNewGame();
		board.setTurn(true); // Forcing PLAYER1
		// Repeat move
		board.play(PLAYER1, 2);
		assertEquals(1, board.getHoles().get(6).intValue());
		assertEquals(PLAYER1, board.getTurn());

		// Second Move, it
		board.play(PLAYER1, 4);
		assertEquals(2, board.getHoles().get(6).intValue());
		assertEquals(5, board.getHoles().get(9).intValue());
		assertEquals(PLAYER2, board.getTurn());
		
		board.play(PLAYER2, 9);
		assertEquals(5, board.getHoles().get(0).intValue());
		assertEquals(PLAYER1, board.getTurn()); 	
		//That's All Folks!
	}
	
	@Test
	public void testJumpSeedHoles() throws Exception {
		board.startNewGame();
		board.setHoles(new ArrayList<Integer>( Arrays.asList(6, 1, 0, 1, 2, 2, 7, 
					2, 2, 0, 11, 9, 0, 5)));
		board.setTurn(false); // Forcing PLAYER2
		//Jump and seed in my holes
		board.play(PLAYER2, 10);
		assertEquals(3, board.getHoles().get(7).intValue());
		assertEquals(7, board.getHoles().get(6).intValue());
		assertEquals(PLAYER1, board.getTurn());
		
		board.play(PLAYER1, 0);
		assertEquals(4, board.getHoles().get(7).intValue());
		assertEquals(8, board.getHoles().get(6).intValue());
		assertEquals(PLAYER2, board.getTurn());
		
	}
	
	@Test
	public void testStealOppositeSeeds() throws Exception {
		
		board.startNewGame();
		board.setHoles(new ArrayList<Integer>( Arrays.asList(5, 5, 0, 6, 5, 5, 1, 
					5, 4, 0, 5, 5, 0, 2)));
		board.setTurn(false); // Forcing PLAYER2
		board.play(PLAYER2, 7);
		assertEquals(0, board.getHoles().get(0).intValue());
		assertEquals(0, board.getHoles().get(12).intValue());
		assertEquals(8, board.getHoles().get(13).intValue());
		assertEquals(PLAYER1, board.getTurn());
		
		board.setHoles(new ArrayList<Integer>( Arrays.asList(0, 6, 1, 0, 6, 6, 1, 
				1, 6, 2, 0, 6, 1, 2)));
		board.play(PLAYER1, 2);
		assertEquals(0, board.getHoles().get(3).intValue());
		assertEquals(0, board.getHoles().get(9).intValue());
	}
	
	@Test
	public void testColletSeeds() throws Exception {
		
		board.startNewGame();
		board.setHoles(new ArrayList<Integer>( Arrays.asList(2, 3, 0, 2, 0, 7, 10, 
					0, 0, 0, 0, 0, 1, 23)));
		board.setTurn(false); // Forcing PLAYER2
		board.play(PLAYER2, 12);
		assertEquals(24, board.getHoles().get(6).intValue());
		assertEquals(24, board.getHoles().get(13).intValue());
		assertEquals("TIE", board.getWinner());
		
	}
	
	@Test
	public void testWinner() throws Exception {
		
		board.startNewGame();
		board.setHoles(new ArrayList<Integer>( Arrays.asList(2, 0, 5, 2, 0, 1, 4, 
					0, 0, 0, 0, 0, 1, 33)));
		board.setTurn(false); // Forcing PLAYER2
		board.play(PLAYER2, 12);
		assertEquals(14, board.getHoles().get(6).intValue());
		assertEquals(34, board.getHoles().get(13).intValue());
		assertEquals(PLAYER2, board.getWinner());
		
	}
}
