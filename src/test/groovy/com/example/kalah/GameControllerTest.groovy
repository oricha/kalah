package com.example.kalah;

import groovy.json.JsonSlurper

import org.springframework.test.web.servlet.MockMvc
import org.springframework.web.client.RestTemplate
import static org.springframework.http.HttpStatus.*
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import spock.lang.Specification


class GameControllerTest extends Specification{

	def controller = new GameController()
	RestTemplate rt
	
	MockMvc mockMvc = standaloneSetup(controller)
	
    def 'setup'(){
        rt = new RestTemplate()
    }
	
	def 'start new game '(){
		
		when:
		 def response = mockMvc.perform(get('/start')).andReturn().response
		 def content = new JsonSlurper().parseText(response.contentAsString)
		
		then:
			response.status == OK.value()
		
	}
}
