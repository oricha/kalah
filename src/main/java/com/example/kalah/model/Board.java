package com.example.kalah.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Board {

	private static final String PLAYER1 = "PLAYER1";
	private static final String PLAYER2 = "PLAYER2";

	private boolean turn;
	private String winner;

	private ArrayList<Integer> holes;

	public void startNewGame() {
		// set all to initial
		setHoles(new ArrayList<Integer>(Arrays.asList(4, 4, 4, 4, 4, 4, 0, 4,
				4, 4, 4, 4, 4, 0)));
		turn = new Random().nextBoolean(); // Select who start
	}

	public void play(String playerName, int hole) {

		int seeds = collectSeeds(hole);
		seed(hole + 1, seeds, playerName);
		turn = !turn; // Change turn player
		
		if(allHolesEmpty()){
			collectSeeds();
			if( holes.get(6).compareTo(holes.get(13)) > 0){
				setWinner(PLAYER1);
			}else if(holes.get(6).compareTo(holes.get(13)) < 0) {
				setWinner(PLAYER2);
			}else{
				setWinner("TIE");
			}
		}
	}

	private void seed(int startIndex, int seeds, String playerName) {
		boolean stealSeeds = false;
		
		while (seeds > 0) {
			Integer item = holes.get(startIndex);

			if (PLAYER1.equalsIgnoreCase(playerName)) {
				if (startIndex == 13) {
					startIndex = 0;
					item = holes.get(startIndex);
				}
				if (startIndex == 6 && seeds == 1) {
					holes.set(startIndex, item + 1);
					seeds--;
					// Forcing player 2,it will change at the end of the method
					setTurn(false);
					continue;
				}
			}
			if (PLAYER2.equalsIgnoreCase(playerName)) {
				if (startIndex == 6) {
					startIndex = 7;
					item = holes.get(startIndex);
				}
				if (startIndex == 13 && seeds == 1) {
					holes.set(startIndex, item + 1);
					seeds--;
					// Forcing player 1,it will change at the end of the method
					setTurn(true);
					continue;
				}
			}
			if (seeds == 1 && 
					item.intValue() == 0 &&
					holes.get(this.getOppositePosition(startIndex)) > 0) {
				stealSeeds = true;
			}
			holes.set(startIndex, item + 1);
			if (stealSeeds) {
				if (PLAYER1.equalsIgnoreCase(playerName)) {
					holes.set(6, holes.get(6)
									+ holes.get(startIndex)
									+ holes.get(this
											.getOppositePosition(startIndex)));
					holes.set(startIndex, 0);
					holes.set(this.getOppositePosition(startIndex), 0);
				}
				if (PLAYER2.equalsIgnoreCase(playerName)) {
					holes.set(13, holes.get(13)
									+ holes.get(startIndex)
									+ holes.get(this
											.getOppositePosition(startIndex)));
					holes.set(startIndex, 0);
					holes.set(this.getOppositePosition(startIndex), 0);
				}
			}
			seeds--;
			if (startIndex == 13) {
				startIndex = 0;
			} else {
				startIndex++;
			}
		}

	}
	
	private int getOppositePosition(int hole) {
		if (hole >= 0 && hole < 6) {
			return ((6 - hole) * 2) + hole;
		}
		return 12 - hole;
	}

	private int collectSeeds(int hole) {
		Integer seed = holes.get(hole);
		holes.set(hole, 0);
		return seed.intValue();

	}

	public String getTurn() {
		return turn ? PLAYER1 : PLAYER2;
	}
	 private boolean allHolesEmpty(){
		 boolean testPLayer1 = true;
		 boolean testPLayer2 = true;
		 int i = 0;
		 while( testPLayer1 && i < 6){
			 if( holes.get(i).intValue() != 0){
				 testPLayer1 = false;
			 }
			 i++;
		 }
		 i = 7;
		 while( testPLayer2 && i < 13){
			 if( holes.get(i).intValue() != 0){
				 testPLayer2 = false;
			 }
			 i++;
		 }
		
		 return testPLayer1 || testPLayer2;
	 }
	 
	 private void collectSeeds(){
		 Integer sum = 0;
		 int i = 0;
		 while( i < 6){
			sum += holes.get(i); 
			holes.set(i, 0);
			i++;
		 }
		 holes.set(6, sum + holes.get(6));
		 
		 sum = 0;
		 i = 7;
		 while( i < 13){
			sum += holes.get(i); 
			holes.set(i, 0);
			i++;
		 }
		 holes.set(13, sum + holes.get(13));
		 
	 }
	/**
	 * Forcing first player
	 * @param value
	 */
	public void setTurn(boolean value) {
		turn = value;
	}


	public ArrayList<Integer> getHoles() {
		return holes;
	}

	public void setHoles(ArrayList<Integer> holes) {
		this.holes = holes;
	}

	public String getWinner() {
		return winner;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}

}
