package com.example.kalah.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.kalah.model.Board;


@Controller
public class GameController {
	
	private Board board;
	
	@RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
	public String home(Model model) {
		model.addAttribute("message", "Press Start");
		return "home";
	}
	
	@RequestMapping(value = { "/start"}, method = RequestMethod.GET)
	public String start(Model model) {
		board = new Board();
		board.startNewGame();
		model.addAttribute("board", this.board);
		model.addAttribute("message", board.getTurn());
		return "home";
	}
	
	@RequestMapping(value = { "/move/{player}/{hole}"}, method = RequestMethod.GET)
	public String move( @PathVariable String player, @PathVariable int hole,Model model) {
		board.play(player, hole);
		model.addAttribute("message", board.getTurn());
		return "home";
	}
	

}
